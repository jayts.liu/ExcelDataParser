package com.jay.base.doc.service;

import java.util.List;

import com.jay.base.dao.AbstractBaseDao;

public class ServiceBroker extends AbstractBaseDao {

	public int[] execQuery(List<String> sqlList) { 
		return getSqlUtil().updateBatch(sqlList);
	}
}
