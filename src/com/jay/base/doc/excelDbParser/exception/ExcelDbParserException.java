package com.jay.base.doc.excelDbParser.exception;

/**
 * 
 * @author Jay (JayTs.Liu@gmail.com)
 * @date 20171228
 * @version 0.1
 */
public class ExcelDbParserException extends Exception {

	public ExcelDbParserException() {
		super();
	}
	
	public ExcelDbParserException(String message) {
		super(message);
	}
	
	public ExcelDbParserException(Throwable t) {
		super(t);
	}
	
	public ExcelDbParserException(String message, Throwable t) {
		super(message, t);
	}
	
}
