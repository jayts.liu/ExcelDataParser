package com.jay.base.doc.excelDbParser.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author Jay (JayTs.Liu@gmail.com)
 * @date 20171222
 * @version 0.1
 */
@XmlAccessorType(XmlAccessType.NONE)
public class Destination {
	
	private String destinationTable;// <destinationTable>target table name</destinationTable>
	private List<Reflect> reflectList; // <reflect>
	
	private int procee;
	private String[] sourceColumn;
	private String[] destinationColumn;
	
	@XmlElement(nillable=false)
	public String getDestinationTable() {
		return destinationTable;
	}

	public void setDestinationTable(String destinationTable) {
		this.destinationTable = destinationTable;
	}

	@XmlElement(name = "reflect")
	public List<Reflect> getReflectList() {
		return reflectList;
	}

	public void setReflectList(List<Reflect> reflectList) {
		this.reflectList = reflectList;
	}

	
	public int getProcee() {
		return procee;
	}

	public void setProcee(int procee) {
		this.procee = procee;
	}

	
	public String[] getSourceColumn() {
		if(sourceColumn == null)
			sourceColumn = new String[reflectList.size()];
			for (int i = 0; i < reflectList.size(); i++) 
				sourceColumn[i] = reflectList.get(i).getSourceColumn();
			
		return sourceColumn;
	}
	
	
	public String[] getDestinationColumn() {
		if(destinationColumn == null)
			destinationColumn = new String[reflectList.size()];
			for (int i = 0; i < reflectList.size(); i++) 
				destinationColumn[i] = reflectList.get(i).getDestinationColumn();
			
		return destinationColumn;
	}
	
	
	public String[] getColumnList(int flowType) {
		if(flowType==1) 
			return getSourceColumn();
		return getDestinationColumn();
	}

}
