package com.jay.base.doc.excelDbParser.model;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author Jay (JayTs.Liu@gmail.com)
 * @date 20171222
 * @version 0.1
 */
public class Source {
	
	private String sourceRootPath;
	private String fileName;// <fileName>source file name</fileName>
	private Integer skipTitleLength;// <skipTitleLength>skip titile length</skipTitleLength>
	private Integer sourceColumnLength;// <sourceColumnLength>source column length</sourceColumnLength>
	private String sourceTable;// <sourceTable>temp table name</sourceTable>
	private String sourceBackupPath;
	private boolean removeSourceFile; //set true to delete source file

	@XmlElement(nillable=false)
	public String getSourceRootPath() {
		return sourceRootPath;
	}

	public void setSourceRootPath(String sourceRootPath) {
		this.sourceRootPath = sourceRootPath;
	}

	@XmlElement(nillable=true)
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@XmlElement(nillable=true)
	public Integer getSkipTitleLength() {
		return skipTitleLength;
	}

	public void setSkipTitleLength(Integer skipTitleLength) {
		this.skipTitleLength = skipTitleLength;
	}

	@XmlElement(nillable=false)
	public Integer getSourceColumnLength() {
		return sourceColumnLength;
	}

	public void setSourceColumnLength(Integer sourceColumnLength) {
		this.sourceColumnLength = sourceColumnLength;
	}

	@XmlElement(nillable=true)
	public String getSourceTable() {
		return sourceTable;
	}

	public void setSourceTable(String sourceTable) {
		this.sourceTable = sourceTable;
	}

	@XmlElement(nillable=true)
	public String getSourceBackupPath() {
		return sourceBackupPath;
	}

	public void setSourceBackupPath(String sourceBackupPath) {
		this.sourceBackupPath = sourceBackupPath;
	}

	@XmlElement(nillable=true)
	public boolean isRemoveSourceFile() {
		return removeSourceFile;
	}

	public void setRemoveSourceFile(boolean removeSourceFile) {
		this.removeSourceFile = removeSourceFile;
	}

}