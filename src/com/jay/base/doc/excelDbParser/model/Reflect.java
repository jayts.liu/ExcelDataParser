package com.jay.base.doc.excelDbParser.model;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author Jay (JayTs.Liu@gmail.com)
 * @date 20171222
 * @version 0.1
 */
public class Reflect {

	private int sequence;
	private String sourceColumn;// <souceColumn>from source column name</souceColumn>
	private String destinationColumn;// <destinationColumn>to target column name</destinationColumn>
	
	@XmlElement(nillable=false)
	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	@XmlElement(nillable=false)
	public String getSourceColumn() {
		return sourceColumn;
	}

	public void setSourceColumn(String sourceColumn) {
		this.sourceColumn = sourceColumn;
	}

	@XmlElement(nillable=false)
	public String getDestinationColumn() {
		return destinationColumn;
	}

	public void setDestinationColumn(String destinationColumn) {
		this.destinationColumn = destinationColumn;
	}

}