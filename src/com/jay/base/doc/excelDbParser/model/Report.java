package com.jay.base.doc.excelDbParser.model;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import com.jay.base.util.common.PropertiesLoder;

/**
 * 
 * @author Jay (JayTs.Liu@gmail.com)
 * @date 20171228
 * @version 0.1
 */
public class Report {

	private final static String REPORT_PATH = PropertiesLoder.getText("res/excelDataParser.properties", "excelDataParser.report.path");
	
	private String templatePath;
	private int updateMinRow;
	private int statusCountS;
	private int statusCountO;
	private int totRowCount;
	private int totInsertRowCount;
	private int totErrCount;
	private List<String> errMsgList;
	private List<ReportDisc> reportDiscList;

	public Report() {}
	
	public Report(String templatePath, int updateMinRow) {
		this.templatePath = templatePath;
		this.updateMinRow = updateMinRow;
	}

	public List<String> getErrMsgList() {
		if (this.errMsgList == null)
			this.errMsgList = new ArrayList<>();
		return this.errMsgList;
	}

	public void setErrMsgList(List<String> errMsgList) {
		this.errMsgList = errMsgList;
	}

	public void addErrMsg(String message) {
		getErrMsgList().add(message);
	}

	public void addErrMsg(Throwable t) {
		addErrMsg("Error：" + t.getMessage());
	}

	public void addErrMsg(String message, Throwable t) {
		addErrMsg(message + " Error：" + t.getMessage());
	}

	public List<ReportDisc> getReportDiscList() {
		if(this.reportDiscList == null)
			this.reportDiscList = new ArrayList<>();
		return this.reportDiscList;
	}

	public void setReportDiscList(List<ReportDisc> reportDiscList) {
		this.reportDiscList = reportDiscList;
	}
	
	public void addReportDisc(ReportDisc reportDisc) {
		getReportDiscList().add(reportDisc);
	}

	public String getTemplatePath() {
		return templatePath;
	}

	public void setTemplatePath(String templatePath) {
		this.templatePath = templatePath;
	}

	public int getUpdateMinRow() {
		return updateMinRow;
	}

	public void setUpdateMinRow(int updateMinRow) {
		this.updateMinRow = updateMinRow;
	}

	public String toReport() {
		StringBuilder sb = new StringBuilder();
		sb.append("\n");
		sb.append(" [Memory Total:" + Runtime.getRuntime().totalMemory() );
    	sb.append(" Free:" + Runtime.getRuntime().freeMemory());
    	sb.append(" Max" + Runtime.getRuntime().maxMemory() + "]\n");
    	sb.append("--------------------------------------------------------\n");
    	sb.append("Template path：" + templatePath + "\n");
    	sb.append("minimum update record/time：" + updateMinRow + "\n");
    	
    	
		int x=0;
		for(ReportDisc rd : getReportDiscList()) {
			sb.append("--------------------------------------------------------\n");
			sb.append((x+=1) + ".\n");
			sb.append("設定檔： " + rd.getConfigName() + "\n");
			sb.append("來源檔： " + rd.getSourceFile()+ "\n");// rowCount isBackup
			
			if(!rd.getBackUpPath().equals("")) {
				sb.append("備份路徑：" + rd.getBackUpPath() +"\n");
				sb.append("資料備份：" + (rd.isBackSuccess() ? "完成" : "未完成") + "\n");
			}
			
			totRowCount += rd.getRowCount();
			totInsertRowCount += rd.getInsertRowCount();
			sb.append("轉入行數(Excel)： " + rd.getRowCount() + "\n");
			sb.append("匯入行數(DB)： " + rd.getInsertRowCount() + "\n");
			sb.append("暫存資料表： " + rd.getTemptable() + "\n"); // rowCount iscomplete
			sb.append("目的資料表： " + rd.getTargetTables() + "\n");// rowCount iscomplete
			
			sb.append("完成狀態： " + rd.getStatus() + "\n");
			if(rd.getStatus().equals("S"))
				statusCountS += 1;
			else
				statusCountO += 1;
				
			totErrCount += rd.getErrMsgList().size();//total err
			sb.append("錯誤訊息 ： " + rd.getErrMsgList().size() + "\n");
			for (int i = 0; i < rd.getErrMsgList().size(); i++)
				sb.append(i + 1 + ") " + rd.getErrMsgList().get(i) + "\n");
		}
		
		sb.append("--------------------------------------------------------\n");
		totErrCount = (totErrCount == 0 ? getErrMsgList().size() : totErrCount);
		sb.append("其他錯誤訊息："+ getErrMsgList().size() +" \n");
		for (int i = 0; i < getErrMsgList().size(); i++)
			sb.append(i + 1 + ") " + getErrMsgList().get(i) + "\n");
		
		sb.append("\n=========================合計===========================\n");
		sb.append(" JOB： " + getReportDiscList().size());
		sb.append("  完成： " + statusCountS);
		sb.append("  其它：" + statusCountO);
		sb.append("  轉入(Excel)： " + totRowCount);
		sb.append("  匯入(DB)： " + totInsertRowCount + "\n ");
		sb.append("  錯誤： " + totErrCount + "\n");
		
		return sb.toString();
	}
	
	/**
	 * display report & explort to file
	 * @throws IOException
	 */
	public void print() throws IOException {
		String rpt = toReport();
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

		Path path = FileSystems.getDefault().getPath(REPORT_PATH);
		if(Files.notExists(path)) //check path exist
			Files.createDirectories(path);
		
		String fileName = sdf.format(new Date()) + String.format("%03d", (new Random().nextInt(1000) + 1));
		Path file = Paths.get(REPORT_PATH + fileName + ".txt");
		Files.write(file, rpt.getBytes());
		
		System.out.println(rpt);//display in console
	}

}

