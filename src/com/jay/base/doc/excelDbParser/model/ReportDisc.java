package com.jay.base.doc.excelDbParser.model;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Jay (JayTs.Liu@gmail.com)
 * @date 20171228
 * @version 0.1
 */
public class ReportDisc {
	
	private String configName;
	private String sourceFile;
	private String temptable;
	private String targetTables;
	private int rowCount;//input row count excel
	private int insertRowCount; 
	private String status;
	private List<String> errMsgList;
	private boolean backSuccess;
	private String backUpPath;
	
	public ReportDisc(ConfigDisc disc, String sourceFile) {
		this.configName = disc.getXmlName();
		this.sourceFile = sourceFile;
		this.temptable = disc.getSource().getSourceTable();
		this.targetTables = disc.getDestinationTableToString();
		this.backUpPath = disc.getSource().getSourceBackupPath();
	}

	public String getConfigName() {
		return configName;
	}

	public void setConfigName(String configName) {
		this.configName = configName;
	}
	
	public String getSourceFile() {
		return sourceFile;
	}

	public void setSourceFile(String sourceFile) {
		this.sourceFile = sourceFile;
	}
	
	public String getTemptable() {
		return temptable;
	}

	public void setTemptable(String temptable) {
		this.temptable = temptable;
	}

	public int getRowCount() {
		return rowCount;
	}

	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}
	
	public void addRowCount() {
		this.rowCount++;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<String> getErrMsgList() {
		if (this.errMsgList == null)
			this.errMsgList = new ArrayList<>();
		return this.errMsgList;
	}

	public void setErrMsgList(List<String> errMsgList) {
		this.errMsgList = errMsgList;
	}

	public void addErrMsg(String message) {
		this.status = "E" + status;
		getErrMsgList().add(message);
	}

	public String getTargetTables() {
		return targetTables;
	}

	public void setTargetTables(String targetTables) {
		this.targetTables = targetTables;
	}

	public boolean isBackSuccess() {
		return backSuccess;
	}

	public void setBackSuccess(boolean backSuccess) {
		this.backSuccess = backSuccess;
	}

	public String getBackUpPath() {
		return backUpPath;
	}

	public void setBackUpPath(String backUpPath) {
		this.backUpPath = backUpPath;
	}

	public int getInsertRowCount() {
		return insertRowCount;
	}

	public void setInsertRowCount(int insertRowCount) {
		this.insertRowCount = insertRowCount;
	}
	
	public void addInsertRowCount(int numberToAdd) {
		this.insertRowCount += numberToAdd;
	}

}
