package com.jay.base.doc.excelDbParser;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.jay.base.doc.excelDbParser.exception.ExcelDbParserException;
import com.jay.base.doc.excelDbParser.model.ConfigDisc;
import com.jay.base.doc.excelDbParser.model.Destination;
import com.jay.base.doc.excelDbParser.model.Report;
import com.jay.base.doc.excelDbParser.model.ReportDisc;
import com.jay.base.doc.excelDbParser.model.Source;
import com.jay.base.doc.service.ServiceBroker;
import com.jay.base.util.common.FileUtil;
import com.jay.base.util.common.FolderUtil;
import com.jay.base.util.common.PropertiesLoder;

/**
 * allow to import excel data to database by configure files
 * @author Jay (JayTs.Liu@gmail.com)
 * @date 20171222
 * @version 0.2
 */
public class ExcelDataParser {
	
	private final static Properties CONFIG = PropertiesLoder.load("res/excelDataParser.properties");
	private final static String TEMP_PATH = CONFIG.getProperty("excelDataParser.import.template.path");
	private final static Integer UPDATE_MIN_ROW = Integer.valueOf(CONFIG.getProperty("excelDataParser.update.min.row")); //update row per time
	
	private ServiceBroker serviceBroker = new ServiceBroker();
	private Report report;

	public ExcelDataParser() {
		initPathInfo();
		//initial config disc
		List<ConfigDisc> configDiscList = parseToConfigDisc(getXmlConfigList());
		processConfigDisc(configDiscList);//start convert
	}
	
	public Report getReport() {
		if(report == null)
			this.report = new Report(TEMP_PATH, UPDATE_MIN_ROW);
		return this.report;
	}
	
	public static void main(String[] args) throws Exception {
		ExcelDataParser ex = new ExcelDataParser();
    	
    	ex.getReport().print();
	}

	private void processConfigDisc(List<ConfigDisc> configDiscList) {
		
		for(ConfigDisc disc : configDiscList) {
				
				String rootPath = disc.getSource().getSourceRootPath();
				String sourceFileName = disc.getSource().getFileName();
				List<String> excelList = null;
				
				//validate
				if(rootPath == null || rootPath.equals("")) {
					addErrMsg(disc.getXmlName()+".sourceRootPath can not be empty! ");
					continue;
				}
				
				//prepare soure file list
				if(sourceFileName != null && !sourceFileName.isEmpty()) {
					excelList = new ArrayList<>();
					excelList.add(rootPath + sourceFileName);
				}
				else {
					excelList = getExcelList(rootPath);
				}
				
				//start parsing
				for(String excel: excelList) {
					try {
						processExcelData(disc, excel);
					} catch (Exception e) {
						e.printStackTrace();
						addErrMsg("Config:" + disc.getXmlName() + " Source:" +excel + " Error:" + e.getMessage());
					} 
				}

		}//end for
	}
	
	private boolean processExcelData(ConfigDisc disc, String excelPath) throws ExcelDbParserException {
		System.out.println("[Start disc:"+disc.getXmlName() + "]");
		Source source = disc.getSource();
		List<Destination> dstLst = disc.getDestinationList();
		ReportDisc reportDisc = new ReportDisc(disc, excelPath);
		
		FileInputStream sourceFile = null;
		Workbook workbook = null;
		try {
			reportDisc.setStatus("I"); //I: Import
			sourceFile = new FileInputStream(new File(excelPath));
			workbook = WorkbookFactory.create(sourceFile);
			//for (int sheetNo = 0; sheetNo < workbook.getNumberOfSheets(); sheetNo++)
	        Sheet sheet = workbook.getSheetAt(0);
	        
	        int skipRow = source.getSkipTitleLength();
            int flowType = (source.getSourceTable().equals("") ? 2 : 1);
	        int rowCnt=0;//row
	        int sqlCnt = 0;
	        List<String> sqlList = new ArrayList<>();
	        
	        reportDisc.setStatus("R");//R:Rows
	        for (Row row : sheet) {
	        	if(skipRow > 0 && rowCnt < skipRow) { //to skip rows
	        		rowCnt++;
	        		continue;
	        	}
	        	
	            List<List<Object>> dataList = new ArrayList<>();
	            List<Object> objLst = new ArrayList<>();
	            int col=0;//column

	            for(int cn=0; cn< row.getLastCellNum(); cn++) {
	            	Cell currentCell = row.getCell(cn, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK );
	            	
	                for(int dl=0; dl < dstLst.size(); dl++) {
	                	if(dataList.isEmpty() || dataList.size() < dstLst.size()) {
	                		objLst = new ArrayList<Object>();
	                		dataList.add(dl,objLst);
	                	}
	                	else { 
	                		objLst = dataList.get(dl);
	                	}
	                	
	                	Destination d = dstLst.get(dl);
	                	int procee = d.getProcee();
	                	int seq = d.getReflectList().get(procee).getSequence()-1; //start from 0
		                if(col==seq) {
		                	//add params
		                	if (currentCell.getCellTypeEnum() == CellType.BLANK) 
		                		objLst.add(null);
			                else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) 
			                	objLst.add(currentCell.getNumericCellValue());
			                else 
			                	objLst.add(currentCell.getStringCellValue());
			                
			                dataList.set(dl, objLst);
			                d.setProcee(((procee+1 < d.getReflectList().size()) ? ++procee : 0));
			                dstLst.set(dl, d);
		                }
	                }
	                col++;
	            }

	            reportDisc.setStatus("Q");//Q:Prepare Query
	            //prepare insert query
	            for(int i=0; i < dstLst.size();i++) {
                	Destination d = dstLst.get(i);
                	String targetTabel = (flowType ==1? source.getSourceTable() : d.getDestinationTable()); 
                	
                	if(d.getColumnList(flowType).length > dataList.get(i).size())//check column length
                		throw new ExcelDbParserException("unexpected column length error (expected:" + d.getColumnList(flowType).length + " , source:" + dataList.get(i).size() +")");
 
                	sqlList.add(getSql(targetTabel, d.getColumnList(flowType), dataList.get(i)));
                	sqlCnt++;
	            }
	            
	            if(sqlCnt >= UPDATE_MIN_ROW) {//check limit exceed 
	            	reportDisc.setStatus("DB");//Database
	            	execQuery(sqlList);// do insert
	            	
	            	reportDisc.addInsertRowCount(sqlCnt);
	            	sqlCnt=0;
	            }
	            
	            rowCnt++;
	            reportDisc.addRowCount();
	        }//end while iterator
	        
	        if(sqlList != null ) { //flush data in trunk
	        	reportDisc.setStatus("FDB");//Database
	        	execQuery(sqlList);
	        	reportDisc.addInsertRowCount(sqlCnt);
	        	sqlCnt=0;
	        }
	        
	        
	        if(flowType==1) { //run query to insert data form temp table to target table
	        	reportDisc.setStatus("T");
	        	doInsertFromTempTable(dstLst, source.getSourceTable());
	        }
	        
	        if(!source.getSourceBackupPath().equals("")) { //backup source file
	        	reportDisc.setStatus("B");
	        	doBackupSourceFile(source);
	        	reportDisc.setBackSuccess(true);
	        }
	        
	        reportDisc.setStatus("S");
	        System.out.println("[End disc:"+disc.getXmlName() + "]");
	        return true;

		}  catch (Exception e) {
			reportDisc.addErrMsg(e.getMessage());
			throw new ExcelDbParserException(e);
		} finally {
			getReport().addReportDisc(reportDisc); //add task rport
			try { if(sourceFile!=null) sourceFile.close(); } catch (IOException e) {}
			try { if(workbook != null) workbook.close(); } catch (IOException e) {}
		} 
	}
	
	private void doInsertFromTempTable(List<Destination> destinationList, String sourceTable) {
		List<String> sqlList = new ArrayList<>();
		for(Destination d: destinationList)
			sqlList.add(getSqlTempToTarget(sourceTable, d));
			
		execQuery(sqlList);
	}

	private void doBackupSourceFile(Source source) {
		List<String> fileList = new ArrayList<>();
		String sourceFile = source.getSourceRootPath()+ source.getFileName();
		String targetFile = source.getSourceBackupPath();
		boolean removeYn = source.isRemoveSourceFile();
		
		File sFile = new File(sourceFile); 
		if(sFile.isDirectory())
			fileList.addAll(FolderUtil.getTreeFiles(sourceFile, "xlsx", "xls"));
		else
			fileList.add(sFile.getPath());
		
		for(String sf : fileList) {
			try {
				FileUtil.copyFile(sf, targetFile+ sf.substring(sf.lastIndexOf("\\")), removeYn);
			} catch (IOException e) {
				e.printStackTrace();
				addErrMsg(e.getMessage());
			}
		}

	}
	
	
////
	private String getSql(String tableName, String[] columnList, List<Object> dataList) {
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO ");
		sql.append(tableName);
		sql.append("(");
		sql.append(Arrays.toString(columnList).replaceAll("[\\[\\]]", ""));
		sql.append(") ");
		sql.append("VALUES(");
		sql.append(getParmasToSqlStr(dataList));
		sql.append("); ");
		return sql.toString();
	}
	
	private String getSqlTempToTarget(String sourceTable, Destination destination) {
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO ");
		sql.append(destination.getDestinationTable());
		sql.append("(");
		sql.append(Arrays.toString(destination.getSourceColumn()).replaceAll("[\\[\\]]", ""));
		sql.append(") ");
		sql.append("SELECT ");
		sql.append(Arrays.toString(destination.getDestinationColumn()).replaceAll("[\\[\\]]", ""));
		sql.append(" FROM ").append(sourceTable).append("; ");

		return sql.toString();
	}
	
	private List<String> getXmlConfigList() {
		return FolderUtil.getTreeFiles(TEMP_PATH, ".xml", ".Xml", ".XML");
	}

	private List<String> getExcelList(String path) {
		return FolderUtil.getTreeFiles(TEMP_PATH, ".xlsx");
	}
	
	private String getParmasToSqlStr(List<Object> dataList) {
		StringBuilder sb = new StringBuilder();
		for(int i=0; i < dataList.size() ; i++) {
			if(i > 0)
				sb.append(", ");
			
			if(dataList.get(i) == null)
				sb.append("NULL");
			else if(dataList.get(i) instanceof Number)
				sb.append(dataList.get(i));
			else
				sb.append("'").append(String.valueOf(dataList.get(i)).replaceAll("'", "")).append("'");
		}
		return sb.toString();
	}
	
	private int execQuery(List<String> sqlList) {
		//for(String s: sqlList) System.out.println(s); //return 0; // for test
		return serviceBroker.execQuery(sqlList).length; 
	}
	
	private List<ConfigDisc> parseToConfigDisc(List<String> xmlList) {
		List<ConfigDisc> accurateDiscList = null;
		
		if (xmlList != null && xmlList.size() > 0) {
			accurateDiscList = new ArrayList<ConfigDisc>();
			for (String fileStr : xmlList) {
				File file = new File(fileStr);
				JAXBContext jaxbContext;
				try {
					jaxbContext = JAXBContext.newInstance(ConfigDisc.class);
				
					Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
					ConfigDisc icd = (ConfigDisc) jaxbUnmarshaller.unmarshal(file);
					icd.setXmlName(file.getName());
					
					accurateDiscList.add(icd);
				} catch (JAXBException e) {
					Throwable t = (e.getMessage() == null ? e.getLinkedException() : e);
					getReport().addErrMsg(fileStr, t);
					e.printStackTrace();
				}
			}
		}

		return accurateDiscList;
	}
	
	private void addErrMsg(String message) {
		getReport().addErrMsg(message);
	}
	
	private void initPathInfo() {

		System.out.println("CONFIG:" + CONFIG);
		System.out.println("TEMP_PATH:" + TEMP_PATH);
		System.out.println("UPDATE_MIN_ROW:" + UPDATE_MIN_ROW);
		
	}

}

